package com.wskj.supermarket.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * 通过C3P0 去配置数据库
 * 
 * @author Administrator
 *
 */
public class ConfigUtil {	
   //static final Logger logger=Logger.getLogger(ConfigUtil.class);
   
	// 实现单例模式
	private static ComboPooledDataSource ds;
	static {
		try {
			ds = new ComboPooledDataSource(); // 使用配置文件中的默认配置
			// ds = new ComboPooledDataSource("oracle");
			// 如果要使用Oracle则使用配置文件中的自定义配置
		} catch (Exception e) {
			//logger.info(e.getMessage());			
			throw new ExceptionInInitializerError(e);
		}
	}

	public static Connection getConnection() throws SQLException {
		return ds.getConnection(); 
	// 使用ComboPooledDataSource对象获取连接
	}

}
