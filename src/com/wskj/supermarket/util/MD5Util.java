package com.wskj.supermarket.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Encoder;
/**
 *  MD5 进行密码的加密 ,防止看到数据库的明文密码 
 * @author Administrator
 *  在各种应用系统中，如果需要设置账户，那么就会涉及到储存用户账户信息的问题，为了保证所储存账户信息的安全，通常会采用MD5加密的方式来，进行储存。首先，简单得介绍一下，什么是MD5加密。
　　MD5的全称是Message-Digest Algorithm 5（信息-摘要算法），在90年代初由MIT Laboratory for Computer Science和RSA Data Security Inc的Ronald L. Rivest开发出来，经MD2、MD3和MD4发展而来。是让大容量信息在用数字签名软件签署私人密匙前被"压缩"成一种保密的格式（就是把一个任意长度的字节串变换成一定长的大整数）。不管是MD2、MD4还是MD5，它们都需要获得一个随机长度的信息并产生一个128位的信息摘要。虽然这些算法的结构或多或少有些相似，但MD2的设计与MD4和MD5完全不同，那是因为MD2是为8位机器做过设计优化的，而MD4和MD5却是面向32位的电脑。这三个算法的描述和C语言源代码在Internet RFCs 1321中有详细的描述，这是一份最权威的文档，由Ronald L. Rivest在1992年8月向IETF提交。

　　（一）消息摘要简介
    一个消息摘要就是一个数据块的数字指纹。即对一个任意长度的一个数据块进行计算，产生一个唯一指印（对于SHA1是产生一个20字节的二进制数组）。消息摘要是一种与消息认证码结合使用以确保消息完整性的技术。主要使用单向散列函数算法，可用于检验消息的完整性，和通过散列密码直接以文本形式保存等，目前广泛使用的算法有MD4、MD5、SHA-1。

消息摘要有两个基本属性：
    两个不同的报文难以生成相同的摘要
    难以对指定的摘要生成一个报文，而可以由该报文反推算出该指定的摘要
代表：美国国家标准技术研究所的SHA1和麻省理工学院Ronald Rivest提出的MD5
 * 
 * 
 * 
 *
 */
public class MD5Util {

	public static  String EncoderByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException{
	 //确定计算方法
	 MessageDigest md5=MessageDigest.getInstance("MD5");
	 BASE64Encoder base64en = new BASE64Encoder();
	 //加密后的字符串
	 String newstr=base64en.encode(md5.digest(str.getBytes("utf-8")));
	 return newstr;
	}

	
	public static void main(String[] args) {
		try {
			System.out.println(MD5Util.EncoderByMd5("zhangsan"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
