package com.wskj.supermarket.util;

import java.lang.reflect.*;
import java.util.*;
import java.sql.*;

/**
 * 将结果集转化为实体类型的辅助类
 * @author Administrator
 *
 */
public class JDBCUtil {
	/**
	 * 将ResultSet转换为泛型集合
	 * @param c   类型
	 * @param set  结果集 
	 * @return
	 *   类型的作用： 
	 *      java 反射 生成 相关的对象 . 无参数的构造方法
	 *         反射 获取所有的属性    
	 *         拼接setter 方法  ，执行setter 方法
	 */
	public static <T> List<T> convertResultSetToList(Class<T> c, ResultSet set){
		List<T> list = new ArrayList<T>();
		//获取所有属性
		Field[] fileds = c.getDeclaredFields();
		try {
			while(set.next()){
				//利用反射自动创建实体对象
				T instance = (T)c.newInstance();
				//遍历所有字段
				for(Field field : fileds){
					invokeFieldSetMethod(instance, field, set);
				}
				list.add(instance);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * 从ResultSet中提取第一条记录，并转换为实体对象
	 * @param c
	 * @param set
	 * @return
	 */
	public static <T> T convertResultSetToEntity(Class<T> c, ResultSet set){
		//获取所有属性
		Field[] fileds = c.getDeclaredFields();
		
		try {
			while(set.next()){
				//利用反射自动创建实体对象
				T instance = (T)c.newInstance();
				//遍历所有字段
				for(Field field : fileds){
					invokeFieldSetMethod(instance, field, set);
				}
				return instance;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 执行字段的setter方法
	 * @param instance
	 * @param field
	 * @param set
	 */
	private static void invokeFieldSetMethod(Object instance, Field field, ResultSet set){
		try{
			//获取字段的setter方法名称
			String methodName = "set"+field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
			//获取字段的setter方法
			Method getMethod = instance.getClass().getMethod(methodName, field.getType());
			
			//获取字段的类型名称（不包含包名）
			String typeName = field.getType().toString().toLowerCase();
			typeName = typeName.substring(typeName.lastIndexOf(".")+1);
			
			if(typeName.equals("int")||typeName.equals("integer")){
				//执行setter方法
				getMethod.invoke(instance, set.getInt(field.getName()));
			}else if(typeName.equals("short")){
				getMethod.invoke(instance, set.getShort(field.getName()));
			}else if(typeName.equals("byte")){
				getMethod.invoke(instance, set.getByte(field.getName()));
			}else if(typeName.equals("long")){
				getMethod.invoke(instance, set.getLong(field.getName()));
			}else if(typeName.equals("float")){
				getMethod.invoke(instance, set.getFloat(field.getName()));
			}else if(typeName.equals("double")){
				getMethod.invoke(instance, set.getDouble(field.getName()));
			}else if(typeName.equals("date")){
				getMethod.invoke(instance, set.getDate(field.getName()));
			}else if(typeName.equals("string")){
				getMethod.invoke(instance, set.getString(field.getName()));
			}else if(typeName.equals("boolean")){
				getMethod.invoke(instance, set.getBoolean(field.getName()));
			}else{
				getMethod.invoke(instance, set.getObject(field.getName()));
			}		
		}catch(Exception ex){
			//ex.printStackTrace();
		}
	}
	
}
