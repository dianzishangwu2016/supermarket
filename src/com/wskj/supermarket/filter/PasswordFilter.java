package com.wskj.supermarket.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import com.wskj.supermarket.util.EncryptionReqestWrapper;

/**
 * 对用户的密码请求  进行统一的md5加密处理 ：
 *     要求统一所有的密码输入框 name属性值 为 password
 * @author Administrator
 *
 */
@WebFilter(urlPatterns={"/*"})
public class PasswordFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub		
		
		request = new EncryptionReqestWrapper((HttpServletRequest) request);
		
       chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
