package com.wskj.supermarket.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

import org.omg.PortableServer.REQUEST_PROCESSING_POLICY_ID;

/**
 *  处理字符编码的过滤器
 * @author Administrator
 *
 */
@WebFilter(urlPatterns={"/*"},
initParams={@WebInitParam(name="encoding",value="utf8")})
public class CharacterEncodingFilter implements Filter {

	private String encoding;  //初始化的字符编码格式
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub		
		encoding=encoding==null?"utf8":encoding;		
		req.setCharacterEncoding(encoding);
		resp.setCharacterEncoding(encoding);		
		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		// TODO Auto-generated method stub		
		encoding=config.getInitParameter("encoding");
	}

}
