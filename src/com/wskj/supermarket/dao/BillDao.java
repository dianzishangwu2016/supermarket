package com.wskj.supermarket.dao;

import java.util.List;

import com.wskj.supermarket.pojo.Bill;

public interface BillDao {

	int insertBill(Bill bill);

	List<Bill> queryAllBills(String gname, int sid, int payed);

	int deleteBillById(int id);

	Bill queryBillById(int id);

	int updateBill(Bill bill);

}
