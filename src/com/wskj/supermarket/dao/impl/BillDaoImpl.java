package com.wskj.supermarket.dao.impl;

import java.util.List;

import com.wskj.supermarket.dao.BillDao;
import com.wskj.supermarket.pojo.Bill;

public class BillDaoImpl extends BaseDao implements BillDao {

	

	@Override
	public List<Bill> queryAllBills(String gname, int sid, int payed) {
		// TODO Auto-generated method stub
		String  sql="select bill.*,provider.`proName` supplierName "
				 + " from bill  inner join provider "
				 + "ON bill.`supplierId`=provider.`id` "
				 + "where  bill.`productName` LIKE CONCAT('%',?,'%') "
				 + "  AND (supplierId=? OR -1=?) "
				 + "  AND (ispayment=? OR -1=?) ";
		Object[] params={gname,sid,sid,payed,payed};
		return super.getAllEntity(sql, params, Bill.class);
	}

	@Override
	public int deleteBillById(int id) {
		// TODO Auto-generated method stub
		String  sql="delete from bill where id=?";
		Object[] params={id};
		return super.executeUpdate(sql, params);
	}

	@Override
	public Bill queryBillById(int id) {
		// TODO Auto-generated method stub
		String  sql="select bill.*,provider.`proName` supplierName "
				+ " from bill "
				+ " inner join provider ON bill.`supplierId`=provider.`id`"
				+ " where bill.id=?";
		Object[] params={id};
		return super.getSingleEntity(sql, params, Bill.class);
	}

	
	@Override
	public int insertBill(Bill bill) {
		// TODO Auto-generated method stub
		String  sql="INSERT INTO bill "
				+ "(billCode,supplierId,productName,productUnit,productCount,totalPrice,isPayment,createdBy,creationDate)"
				+ " VALUES (?,?,?,?,?,?,?,?,?);";
		Object[] params={bill.getBillCode(),bill.getSupplierId(),bill.getProductName(),bill.getProductUnit(),
				     bill.getProductCount(),bill.getTotalPrice(),bill.getIsPayment(),bill.getCreatedBy(),bill.getCreationDate()};
		return super.executeUpdate(sql, params);
	}
	
	
	@Override
	public int updateBill(Bill bill) {
		// TODO Auto-generated method stub
		String  sql="UPDATE bill SET billcode=?,supplierId=?,productName=?,"
				+ " productUnit=?,productCount=?,totalPrice=?,isPayment=?,"
				+ "  modifyBy=?,modifyDate=?  WHERE id=? ";
				
		Object[] params={bill.getBillCode(),bill.getSupplierId(),bill.getProductName(),bill.getProductUnit(),
			     bill.getProductCount(),bill.getTotalPrice(),bill.getIsPayment(),bill.getModifyBy(),bill.getModifyDate(),bill.getId()};;
		return super.executeUpdate(sql, params);
	}

	
	
}
