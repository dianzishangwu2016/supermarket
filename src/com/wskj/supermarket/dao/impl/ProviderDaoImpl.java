package com.wskj.supermarket.dao.impl;

import java.util.List;

import com.wskj.supermarket.dao.ProviderDao;
import com.wskj.supermarket.pojo.Provider;

public class ProviderDaoImpl extends BaseDao implements ProviderDao {

	@Override
	public Provider queryProviderById(Integer id) {
		// TODO Auto-generated method stub
		String sql="select * from provider where id=?";
		Object[] params={id};
		return super.getSingleEntity(sql, params, Provider.class);
	}

	@Override
	public List<Provider> queryAllProviders(String name) {
		// TODO Auto-generated method stub
		String sql="select * from provider where proName like concat('%',?,'%')";
		Object[] params={name};
		return super.getAllEntity(sql, params,Provider.class);
	
	}

	@Override
	public int deleteProviderById(Integer id) {
		// TODO Auto-generated method stub
		String sql="delete from provider where id=?";
		Object[] params={id};
		return super.executeUpdate(sql, params);
	}

	@Override
	public int insertProvider(Provider pro) {
		// TODO Auto-generated method stub
		String sql="insert into provider "
				+ " (proCode,proName,prodesc,procontact,prophone,proAddress,proFax,createdBy,creationDate) "
				+ "values (?,?,?,?,?,?,?,?,?)";
		Object[] params={pro.getProCode(),pro.getProName(),pro.getProDesc(),pro.getProContact(),
				pro.getProPhone(),pro.getProAddress(),pro.getProFax(),pro.getCreatedBy(),pro.getCreationDate()};
		return super.executeUpdate(sql, params);
	}

	@Override
	public int updateProvider(Provider pro) {
		// TODO Auto-generated method stub
		String sql="update provider set procode=?,proname=?,prodesc=?,"
				+ " procontact=?,prophone=?,proAddress=?,profax=?,modifyby=?,modifyDate=?  where id=?";
		Object[] params={pro.getProCode(),pro.getProName(),pro.getProDesc(),pro.getProContact(),
				pro.getProPhone(),pro.getProAddress(),pro.getProFax(),pro.getModifyBy(),pro.getModifyDate(),pro.getId()};
		return super.executeUpdate(sql, params);
	}

}
