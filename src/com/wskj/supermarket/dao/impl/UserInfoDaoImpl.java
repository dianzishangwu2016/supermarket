package com.wskj.supermarket.dao.impl;

import java.util.List;

import com.wskj.supermarket.dao.UserInfoDao;
import com.wskj.supermarket.pojo.UserInfo;

public class UserInfoDaoImpl extends BaseDao implements UserInfoDao {
    
	
	@Override
	public List<UserInfo> queryAllUsersByName(String name) {
		// TODO Auto-generated method stub
		String sql="select *,TIMESTAMPDIFF(YEAR, birthday, CURDATE()) age,typeName"
				+ " from userinfo "
				+ " inner join usertype on userinfo.usertype=usertype.typeid "
				+ "where username like concat('%',?,'%')";
		Object[] params={name};
		return super.getAllEntity(sql, params, UserInfo.class);
	}

	
	@Override
	public UserInfo checkLogin(String username, String password) {
		// TODO Auto-generated method stub
		String sql="select *,TIMESTAMPDIFF(YEAR, birthday, CURDATE()) age "
				+ " from userinfo "				
				+ " where usercode=? and userpassword=?";
		Object[] params={username,password};
		return super.getSingleEntity(sql, params, UserInfo.class);
	}


	@Override
	public boolean deleteById(Integer userId) {
		// TODO Auto-generated method stub
		String sql="delete from userinfo where id=?";
		Object[] params={userId};
		
		int count = super.executeUpdate(sql, params);
		
		
		return count>0?true:false;
	}


	
	@Override
	public int getCountsByUserCode(String userCode) {
		// TODO Auto-generated method stub
		String sql="select count(1) from userinfo where usercode=?";
		Object[] params={userCode};		
		return super.getCounts(sql, params);
	}


	
	@Override
	public boolean insertUser(UserInfo user) {
		// TODO Auto-generated method stub
		String sql=" insert into userinfo (userCode,userName,userpassword,gender,birthday,phone,address,userType,createdBy,creationDate)  "
				+ " values (?,?,?,?,?,?,?,?,?,?)";
		Object[] params={user.getUserCode(),user.getUserName(),user.getUserPassword(),
				user.getGender(),user.getBirthday(),user.getPhone(),user.getAddress(),
				user.getUserType(),user.getCreatedBy(),user.getCreationDate()};
		int count=super.executeUpdate(sql, params);
		return count>0?true:false;
	}


	@Override
	public UserInfo queryUserInfoById(Integer id) {
		// TODO Auto-generated method stub
		String sql="select * from userinfo where id=?";
		Object[] params={id};
		return super.getSingleEntity(sql, params, UserInfo.class);
	}


	@Override
	public int updateUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
		String sql="update userinfo set userName=?,gender=?,birthday=?,"
				+ "phone=?,userType=?,address=?,modifyby=?,modifydate=? "
				+ "where id=?";
		Object[] params={user.getUserName(),user.getGender(),user.getBirthday(),
				user.getPhone(),user.getUserType(),user.getAddress(),
				user.getModifyBy(),user.getModifyDate(),user.getId()};
		return super.executeUpdate(sql, params);
	}


	@Override
	public int changePassword(long uid, String newPass) {
		// TODO Auto-generated method stub
		String  sql="update userinfo set userpassword=?  where id=?";
		Object[] params={newPass,uid};
		return super.executeUpdate(sql, params);
	}
    
	
	
	
	 
}
