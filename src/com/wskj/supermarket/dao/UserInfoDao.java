package com.wskj.supermarket.dao;

import java.util.List;

import com.wskj.supermarket.pojo.UserInfo;

public interface UserInfoDao {
    
	/**
	 * 根据用户名查满足条件的用户的信息 
	 * @param name 用户的名称 
	 * @return
	 */
	List<UserInfo> queryAllUsersByName(String name);

	/**
	 *  检查登陆
	 * @param username 账号
	 * @param password 密码
	 * @return
	 */
	UserInfo checkLogin(String username, String password);

	/**
	 *  通过id 删除用户信息
	 * @param userId 用户编号
	 * @return
	 */
	boolean deleteById(Integer userId);

	/**
	 * 查询 用户编号的个数
	 * @param userCode
	 * @return
	 */
	int getCountsByUserCode(String userCode);

	/**
	 *  新增用户
	 * @param user
	 * @return
	 */
	boolean insertUser(UserInfo user);

	/**
	 *  根据id 查询用户的信息
	 * @param id
	 * @return
	 */
	 UserInfo queryUserInfoById(Integer id);

	 /**
	  *  执行修改数据 
	  * @param user
	  * @return
	  */
	int updateUserInfo(UserInfo user);

	int changePassword(long uid, String newPass); 

}
