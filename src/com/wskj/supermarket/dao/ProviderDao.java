package com.wskj.supermarket.dao;

import java.util.List;

import com.wskj.supermarket.pojo.Provider;

public interface ProviderDao {

	Provider queryProviderById(Integer id);

	List<Provider> queryAllProviders(String name);

	int deleteProviderById(Integer id);

	int insertProvider(Provider provider);

	int updateProvider(Provider provider);

}
