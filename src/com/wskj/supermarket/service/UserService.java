package com.wskj.supermarket.service;

import java.util.List;

import com.wskj.supermarket.pojo.UserInfo;

/**
 * 用户信息的服务层
 * @author Administrator
 *
 */
public interface UserService {
    
	/**
	 * 根据用户的姓名查询所有的用户的信息
	 * @param name 用户的姓名
	 * @return
	 */
	List<UserInfo> queryAllUsersByName(String name);

	/**
	 * 通过账号和密码查询用户，登陆的判断
	 * @param username 账号
	 * @param password 密码
	 * @return
	 */
	UserInfo getUserByNameAndPassword(String username, String password);

	/**
	 * 通过用户的编号删除用户
	 * @param userId 用户的编号
	 * @return
	 */
	boolean deleteUserById(Integer userId);

	/**
	 * 判断用户编码是否已经存在   true 存在  false 表示不存在
	 * @param userCode
	 * @return
	 */
	boolean checkUserCode(String userCode);

	/**
	 * 新增用户 
	 * @param addUser 新增用户
	 * @return
	 */
	boolean insertUser(UserInfo user);
    
	/**
	 * 通过用户编号查询用户
	 * @param userId
	 * @return
	 */
	UserInfo queryUserInfoById(Integer userId);

	

	/**
	 *  修改用户的信息
	 * @param userInfo
	 * @return
	 */
	boolean updateUserInfo(UserInfo userInfo);

	/**
	 * 更用户的密码
	 * @param uid
	 * @param newPass
	 * @return
	 */
	boolean changePassword(long uid, String newPass);
    
	
}
