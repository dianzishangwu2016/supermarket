package com.wskj.supermarket.service;

import java.util.List;

import com.wskj.supermarket.pojo.Bill;
import com.wskj.supermarket.pojo.Provider;

public interface BillService {
    
	List<Provider> queryAllProviders(String name);

	Provider queryProviderById(Integer id);

	boolean deleteProviderById(Integer id);

	boolean insertProvider(Provider provider);

	boolean updateProvider(Provider provider);

	Bill queryBillById(int id);

	boolean deleteBillById(int id);

	boolean insertBill(Bill bill);

	List<Bill> queryAllBills(String gname, int sid, int payed);

	boolean updateBill(Bill bill);

}
