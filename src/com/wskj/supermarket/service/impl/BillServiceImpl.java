package com.wskj.supermarket.service.impl;

import java.util.List;

import com.wskj.supermarket.dao.BillDao;
import com.wskj.supermarket.dao.ProviderDao;
import com.wskj.supermarket.dao.impl.BillDaoImpl;
import com.wskj.supermarket.dao.impl.ProviderDaoImpl;
import com.wskj.supermarket.pojo.Bill;
import com.wskj.supermarket.pojo.Provider;
import com.wskj.supermarket.service.BillService;

public  class BillServiceImpl implements BillService {
	
	private BillDao billDao=new BillDaoImpl();//处理账单表的相关信息 
	
	private ProviderDao providerDao=new ProviderDaoImpl();//处理供应商表的相关信息

	@Override
	public List<Provider> queryAllProviders(String name) {
		// TODO Auto-generated method stub
		return providerDao.queryAllProviders(name);
	}

	@Override
	public Provider queryProviderById(Integer id) {
		// TODO Auto-generated method stub
		return providerDao.queryProviderById(id);
	}

	@Override
	public boolean deleteProviderById(Integer id) {
		// TODO Auto-generated method stub
		int count=providerDao.deleteProviderById(id);
		return count>0?true:false;
	}

	@Override
	public boolean insertProvider(Provider provider) {
		// TODO Auto-generated method stub
		int count=providerDao.insertProvider(provider);
		
		return count>0?true:false;
	}

	@Override
	public boolean updateProvider(Provider provider) {
		// TODO Auto-generated method stub
		int count=providerDao.updateProvider(provider);
		
		return count>0?true:false;
	}

	
	@Override
	public Bill queryBillById(int id) {
		// TODO Auto-generated method stub
		return billDao.queryBillById(id);
	}
	
	

	@Override
	public boolean deleteBillById(int id) {
		// TODO Auto-generated method stub
		int count=billDao.deleteBillById(id);
		return count>0?true:false;
	}

	@Override
	public boolean insertBill(Bill bill) {
		// TODO Auto-generated method stub
		int count=billDao.insertBill(bill);		
		return count>0?true:false;
	}

	@Override
	public List<Bill> queryAllBills(String gname, int sid, int payed) {
		// TODO Auto-generated method stub
		return billDao.queryAllBills(gname,sid,payed);
	}

	@Override
	public boolean updateBill(Bill bill) {
		// TODO Auto-generated method stub
		int count=billDao.updateBill(bill);
		return count>0?true:false;
	}
	
	
	

}
