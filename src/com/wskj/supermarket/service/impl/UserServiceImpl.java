package com.wskj.supermarket.service.impl;

import java.util.List;

import com.wskj.supermarket.dao.UserInfoDao;
import com.wskj.supermarket.dao.UserTypeDao;
import com.wskj.supermarket.dao.impl.UserInfoDaoImpl;
import com.wskj.supermarket.dao.impl.UserTypeDaoImpl;
import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;

public  class UserServiceImpl implements UserService {
    
	private UserInfoDao userInfoDao =new UserInfoDaoImpl(); // 处理用户表的dao
	private UserTypeDao typeDao=new UserTypeDaoImpl(); //处理用户类型的dao
	
	
	
	
	@Override
	public List<UserInfo> queryAllUsersByName(String name) {
		// TODO Auto-generated method stub
		return userInfoDao.queryAllUsersByName(name);
	}




	@Override
	public UserInfo getUserByNameAndPassword(String username, String password) {
		// TODO Auto-generated method stub
		return userInfoDao.checkLogin(username,password);
	}




	@Override
	public boolean deleteUserById(Integer userId) {
		// TODO Auto-generated method stub
		return userInfoDao.deleteById(userId);
	}




	@Override
	public boolean checkUserCode(String userCode) {
		// TODO Auto-generated method stub
		int count=userInfoDao.getCountsByUserCode(userCode);
		
		return  count>0?true:false ;
	}




	@Override
	public boolean insertUser(UserInfo user) {
		// TODO Auto-generated method stub
		return userInfoDao.insertUser(user);
	}




	@Override
	public UserInfo queryUserInfoById(Integer id) {
		// TODO Auto-generated method stub
		return userInfoDao.queryUserInfoById(id);
	}




	



	@Override
	public boolean updateUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
   int count= userInfoDao.updateUserInfo(user);
		return count>0?true:false;
	}




	@Override
	public boolean changePassword(long uid, String newPass) {
		// TODO Auto-generated method stub
		int count=userInfoDao.changePassword(uid,newPass);
		return count>0?true:false;
	}

	
	
}
