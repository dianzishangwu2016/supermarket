package com.wskj.supermarket.controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;

@WebServlet(urlPatterns={"/user/list.do"})
public class UserListServlet extends HttpServlet {
	  
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String name=req.getParameter("uname");// 用户名 
		// 查询所有的用户信息
		
		name=name==null?"":name;
		List<UserInfo> users=service.queryAllUsersByName(name);
		
		//存储用户信息
		req.setAttribute("users",users);
		//转发到用户列表的页面
		req.getRequestDispatcher("/WEB-INF/page/users/userList.jsp").forward(req, resp);
	}
	

}
