package com.wskj.supermarket.controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;

@WebServlet("/updatePassword.do")
public class UpdatePasswordServlet extends HttpServlet {
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		req.getRequestDispatcher("/WEB-INF/page/updatePassword.jsp").forward(req, resp);
		
	}
	
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException ,IOException {
		
		
		UserInfo user =(UserInfo) req.getSession().getAttribute("user");
		
		long uid=user.getId();
		String  pass=user.getUserPassword();
		String[] passwords = req.getParameterValues("password");
		
		String oldPass =passwords[0];
		String newPass = passwords[1];
		
		if(pass.equals(oldPass)){
			
			boolean result=service.changePassword(uid,newPass);
			
			
			resp.sendRedirect(req.getContextPath()+"/loginOut.do");
			
		}else{
			
			
			req.getRequestDispatcher("/WEB-INF/page/updatePassword.jsp").forward(req, resp);
		}
		
		
		
		
		
	};

}
