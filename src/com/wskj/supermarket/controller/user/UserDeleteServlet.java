package com.wskj.supermarket.controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;

@WebServlet(urlPatterns={"/user/delete.do"})
public class UserDeleteServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Integer userId=Integer.parseInt(req.getParameter("id"));
		
		boolean result=service.deleteUserById(userId);
		
		// 显示是否删除成功！
		String mess=result?"删除成功！":"删除失败！";//提示信息 
		String  url=req.getContextPath()+"/user/list.do";//跳转地址 
		
		resp.setContentType("text/html;charset=utf8");
		PrintWriter out = resp.getWriter();
		
		out.write("<script>	alert('"+mess+"');window.location.href='"+url+"';</script>");
		
		
	}
	
	
}
