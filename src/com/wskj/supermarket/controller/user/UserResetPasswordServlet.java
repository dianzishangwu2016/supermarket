package com.wskj.supermarket.controller.user;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;
import com.wskj.supermarket.util.MD5Util;

@WebServlet(urlPatterns = {"/user/resetPassword.do" })
public class UserResetPasswordServlet extends HttpServlet {
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		long  uid=Long.parseLong(req.getParameter("id"));
		try {
			service.changePassword(uid, MD5Util.EncoderByMd5("000000"));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		resp.sendRedirect(req.getContextPath()+"/user/list.do");		
	}
	
}
