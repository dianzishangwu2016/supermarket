package com.wskj.supermarket.controller.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;

@WebServlet(urlPatterns={"/user/checkUserCode.do"})
public class UserCodeCheckServlet extends HttpServlet {
	
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
	  	
		String  userCode=req.getParameter("code");
		
		boolean result=service.checkUserCode(userCode);
		
		resp.setContentType("application/json");
		
		PrintWriter out = resp.getWriter();
	   
		out.print(result);
	}

	
	
}
