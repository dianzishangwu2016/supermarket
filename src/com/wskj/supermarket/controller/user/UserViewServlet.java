package com.wskj.supermarket.controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.beans.editors.IntegerEditor;
import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;

@WebServlet(urlPatterns={"/user/view.do"})
public class UserViewServlet extends HttpServlet {
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer userId=Integer.parseInt(req.getParameter("id"));
		
		UserInfo  userInfo=service.queryUserInfoById(userId);
		
		req.setAttribute("user",userInfo);
		
		
		
		req.getRequestDispatcher("/WEB-INF/page/users/userView.jsp").forward(req, resp);
	}
	
}
