package com.wskj.supermarket.controller.user;

import java.util.Date;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;
import com.wskj.supermarket.util.BirthdayUtil;

@WebServlet(urlPatterns = { "/user/add.do" })
public class UserAddServlet extends HttpServlet {
	private UserService  service=new UserServiceImpl();// 用户服务层
	
	protected void doGet(javax.servlet.http.HttpServletRequest req, javax.servlet.http.HttpServletResponse resp)
			throws javax.servlet.ServletException, java.io.IOException {
				
		req.getRequestDispatcher("/WEB-INF/page/users/userAdd.jsp").forward(req, resp);
	};

	
	
	protected void doPost(javax.servlet.http.HttpServletRequest req, javax.servlet.http.HttpServletResponse resp)
			throws javax.servlet.ServletException, java.io.IOException {
		
		
		String  userCode=req.getParameter("userId");
        String userName = req.getParameter("userName");
        String userPass = req.getParameter("password");
        int gender =Integer.parseInt( req.getParameter("gender"));
        String date = req.getParameter("date");
        //将字符串转化为时间
        Date birthday=BirthdayUtil.convertStringToDate(date);
        String phone = req.getParameter("userphone");
        int userType =Integer.parseInt( req.getParameter("userlei"));
        String address = req.getParameter("userAddress");
        
        UserInfo user=(UserInfo)req.getSession().getAttribute("user");
        long userid=user==null?1:user.getId();
		
        
        UserInfo  addUser=new UserInfo(userCode,userName,userPass,gender,birthday,phone,address,userType,userid,new Date());    
		        
		
		boolean result=service.updateUserInfo(addUser);
		
		
		resp.sendRedirect(req.getContextPath()+"/user/list.do");
	};
}
