package com.wskj.supermarket.controller.user;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.UserService;
import com.wskj.supermarket.service.impl.UserServiceImpl;
import com.wskj.supermarket.util.BirthdayUtil;

@WebServlet(urlPatterns={"/user/update.do"})
public class UserUpdateServlet extends HttpServlet {
	private UserService  service=new UserServiceImpl();// 用户服务层
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
       Integer userId=Integer.parseInt(req.getParameter("id"));
		
		UserInfo  userInfo=service.queryUserInfoById(userId);
		
		req.setAttribute("user",userInfo);
		req.getRequestDispatcher("/WEB-INF/page/users/userUpdate.jsp").forward(req, resp);
	}

	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    
		long  id=Long.parseLong(req.getParameter("id"));
        String userName = req.getParameter("userName");       
        int gender =Integer.parseInt( req.getParameter("gender"));
        String date = req.getParameter("date");
        //将字符串转化为时间
        Date birthday=BirthdayUtil.convertStringToDate(date);
        String phone = req.getParameter("userphone");
        int userType =Integer.parseInt( req.getParameter("userlei"));
        String address = req.getParameter("userAddress");
        
        UserInfo user=(UserInfo)req.getSession().getAttribute("user");
        long userid=user==null?1:user.getId();
		
		UserInfo userInfo=new UserInfo(id,userName,gender,birthday,
				phone, address, userType, 
				userid, new Date());
		
		
		
		boolean result=service.updateUserInfo(userInfo);
		resp.sendRedirect(req.getContextPath()+"/user/list.do");
	
	};

}
