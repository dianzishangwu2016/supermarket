package com.wskj.supermarket.controller.bill;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.Bill;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@WebServlet(urlPatterns={"/bill/delete.do"})
public class BillDeleteServlet extends HttpServlet {
	private  BillService service=new BillServiceImpl();
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       int id=Integer.parseInt(req.getParameter("id"));
		
		boolean  result=service.deleteBillById(id);	
		
		resp.sendRedirect(req.getContextPath()+"/bill/list.do");
	}
	
}
