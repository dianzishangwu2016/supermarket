package com.wskj.supermarket.controller.bill;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.Bill;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@WebServlet(urlPatterns={"/bill/view.do"})
public class BillViewServlet extends HttpServlet {
	private  BillService service=new BillServiceImpl();
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id=Integer.parseInt(req.getParameter("id"));
		
		Bill bill=service.queryBillById(id);
		
		req.setAttribute("bill",bill);
		
		
		req.getRequestDispatcher("/WEB-INF/page/bill/billView.jsp").forward(req, resp);
		
		
	}
}
