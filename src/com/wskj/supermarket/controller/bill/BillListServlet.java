package com.wskj.supermarket.controller.bill;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.wskj.supermarket.pojo.Bill;
import com.wskj.supermarket.pojo.Provider;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@WebServlet(urlPatterns={"/bill/list.do"})
public class BillListServlet extends HttpServlet {
	private  BillService service=new BillServiceImpl();
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String gname = req.getParameter("gname");
		String cid = req.getParameter("sid");
		String ispay = req.getParameter("fukuan");	
		
		gname=gname==null?"":gname;
		int sid=cid==null?-1:Integer.parseInt(cid);
		int payed=ispay==null?-1:Integer.parseInt(ispay); 
		
		
		
        List<Provider> providers=service.queryAllProviders("");		
		req.setAttribute("providers",providers);
		
		List<Bill> bills=service.queryAllBills(gname,sid,payed);
		
		req.setAttribute("bills", bills);
		
		req.getRequestDispatcher("/WEB-INF/page/bill/billList.jsp").forward(req, resp);
		
	}

}
