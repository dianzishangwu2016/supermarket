package com.wskj.supermarket.controller.bill;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.Bill;
import com.wskj.supermarket.pojo.Provider;
import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@WebServlet(urlPatterns = { "/bill/update.do" })
public class BillUpdateServlet extends HttpServlet {
	private  BillService service=new BillServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
       List<Provider> providers=service.queryAllProviders("");
		
		
		req.setAttribute("providers",providers);
		
		
        int id=Integer.parseInt(req.getParameter("id"));
		
		Bill bill=service.queryBillById(id);
		
		req.setAttribute("bill",bill);
		
		req.getRequestDispatcher("/WEB-INF/page/bill/billUpdate.jsp").forward(req, resp);
	}

	
	
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	      
		  long  bid=Long.parseLong(req.getParameter("id"));
		  String code = req.getParameter("billId");
		  String gname = req.getParameter("billName");
		  String unit = req.getParameter("billCom");
		  double count =Double.parseDouble( req.getParameter("billNum"));
		  double cost = Double.parseDouble(req.getParameter("money"));
		  int sid =Integer.parseInt( req.getParameter("supplier"));
		  int payed =Integer.parseInt(  req.getParameter("zhifu"));
		  
		  UserInfo userInfo=(UserInfo)req.getSession().getAttribute("user");
		  long uid=userInfo==null?1:userInfo.getId();
		
		  Bill bill =new Bill(bid, code, gname, sid, unit,count,cost,payed,uid, new Date());
		
		  boolean result=service.updateBill(bill);
		
		
		resp.sendRedirect(req.getContextPath()+"/bill/list.do");
	};
	
	
}
