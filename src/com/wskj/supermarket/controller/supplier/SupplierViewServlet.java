package com.wskj.supermarket.controller.supplier;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.Provider;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@WebServlet(urlPatterns={"/supplier/view.do"})
public class SupplierViewServlet extends HttpServlet {
	private  BillService service=new BillServiceImpl();
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer id=Integer.parseInt(req.getParameter("id"));
		Provider provider=service.queryProviderById(id);
		
		req.setAttribute("pro",provider);	
		
		req.getRequestDispatcher("/WEB-INF/page/supplier/providerView.jsp").forward(req, resp);
		
	}
	
}
