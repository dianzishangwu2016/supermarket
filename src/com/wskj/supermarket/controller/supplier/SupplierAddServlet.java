package com.wskj.supermarket.controller.supplier;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wskj.supermarket.pojo.Provider;
import com.wskj.supermarket.pojo.UserInfo;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@WebServlet(urlPatterns = { "/supplier/add.do" })
public class SupplierAddServlet extends HttpServlet {
	private  BillService service=new BillServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				
		req.getRequestDispatcher("/WEB-INF/page/supplier/providerAdd.jsp").forward(req, resp);
	}

	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		String code = req.getParameter("providerCode");
		String name = req.getParameter("providerName");
		String concat = req.getParameter("proConcat");
		String phone = req.getParameter("phone");
		String address = req.getParameter("address");
		String fax = req.getParameter("fax");
		String desc = req.getParameter("describe");
		
		UserInfo userInfo=(UserInfo)req.getSession().getAttribute("user");
		long uid=userInfo==null?1:userInfo.getId();
		Provider provider=new Provider(code,name,desc,concat,phone,address,fax,uid,new Date());
	
		
		boolean result=service.insertProvider(provider);
		
		resp.sendRedirect(req.getContextPath()+"/supplier/list.do");
	};
}
