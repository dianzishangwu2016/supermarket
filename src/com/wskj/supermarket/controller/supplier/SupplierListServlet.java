package com.wskj.supermarket.controller.supplier;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.wskj.supermarket.pojo.Provider;
import com.wskj.supermarket.service.BillService;
import com.wskj.supermarket.service.impl.BillServiceImpl;

@SuppressWarnings("serial")
@WebServlet(urlPatterns={"/supplier/list.do"})
public class SupplierListServlet extends HttpServlet {
	
	private  BillService service=new BillServiceImpl();
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String name=req.getParameter("name");
		name=name==null?"":name;
		List<Provider> list=service.queryAllProviders(name);
		
		req.setAttribute("list",list);
		
		req.getRequestDispatcher("/WEB-INF/page/supplier/providerList.jsp").forward(req, resp);
		
		
	}

}
