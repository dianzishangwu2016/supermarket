package com.wskj.supermarket.pojo;

/**
 * UserType 实体类
 * Wed Oct 18 13:57:23 CST 2017 
 */ 
public class UserType{
	private int typeId;//类型的编号
	private String typeName;//类型名称
	
	public void setTypeId(int typeId){
	this.typeId=typeId;
	}
	public int getTypeId(){
		return typeId;
	}
	public void setTypeName(String typeName){
	this.typeName=typeName;
	}
	public String getTypeName(){
		return typeName;
	}
}

