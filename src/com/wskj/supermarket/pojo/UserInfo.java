package com.wskj.supermarket.pojo;

import java.util.Date;

/**
 * UserInfo 实体类 Tue Oct 17 18:24:14 CST 2017
 */
public class UserInfo {
	private long id; // 用户编号
	private String userCode;// 账户
	private String userName;// 真实姓名
	private String userPassword;// 密码
	private int gender; // 1.男; 2.女

	private Date birthday;// 生日
	private int age;
	private String phone;// 电话
	private String address;// 练习地址
	private int userType; // 用户类型 1.总经理; 2,部门经理 ;3.普通员工
	private String typeName;
	private long createdBy;// 创建者
	private Date creationDate;// 创建时间
	private long modifyBy;// 修改者
	private Date modifyDate;// 修改的时间

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getGender() {
		return gender;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public int getUserType() {
		return userType;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setModifyBy(long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public long getModifyBy() {
		return modifyBy;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public UserInfo(long id, String userCode, String userName, String userPassword, int gender, Date birthday,
			String phone, String address, int userType, long createdBy, Date creationDate, long modifyBy,
			Date modifyDate) {
		super();
		this.id = id;
		this.userCode = userCode;
		this.userName = userName;
		this.userPassword = userPassword;
		this.gender = gender;
		this.birthday = birthday;
		this.phone = phone;
		this.address = address;
		this.userType = userType;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
		this.modifyBy = modifyBy;
		this.modifyDate = modifyDate;
	}

	public UserInfo() {
		super();
	}

	public UserInfo(String userCode, String userName, String userPassword, int gender, Date birthday, String phone,
			String address, int userType, long createdBy, Date creationDate) {
		super();
		this.userCode = userCode;
		this.userName = userName;
		this.userPassword = userPassword;
		this.gender = gender;
		this.birthday = birthday;
		this.phone = phone;
		this.address = address;
		this.userType = userType;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
	}

	public UserInfo(long id, String userName,int gender, Date birthday,
			String phone, String address, int userType, 
			long modifyBy, Date modifyDate) {
		super();
		this.id = id;		
		this.userName = userName;		
		this.gender = gender;
		this.birthday = birthday;		
		this.phone = phone;
		this.address = address;
		this.userType = userType;		
		this.modifyBy = modifyBy;
		this.modifyDate = modifyDate;
	}
	
	

}
