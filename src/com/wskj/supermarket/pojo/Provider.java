package com.wskj.supermarket.pojo;

import java.util.Date;

/**
 * Provider 实体类  
 * Tue Oct 17 18:23:56 CST 2017 
 */ 
public class Provider{
	private long id;//供应商的编号
	private String proCode;//供应商的编码
	private String proName;//供应商的名称
	private String proDesc;//供应商的描述
	private String proContact;//供应联系人
	private String proPhone;//供应商联系电话
	private String proAddress;//联系地址
	private String proFax;//传真
	private long createdBy;//创建人
	private Date creationDate;//创建时间
	private Date modifyDate;//修改时间
	private long modifyBy;//修改人
	
	
	public void setId(long id){
	this.id=id;
	}
	public long getId(){
		return id;
	}
	public void setProCode(String proCode){
	this.proCode=proCode;
	}
	public String getProCode(){
		return proCode;
	}
	public void setProName(String proName){
	this.proName=proName;
	}
	public String getProName(){
		return proName;
	}
	public void setProDesc(String proDesc){
	this.proDesc=proDesc;
	}
	public String getProDesc(){
		return proDesc;
	}
	public void setProContact(String proContact){
	this.proContact=proContact;
	}
	public String getProContact(){
		return proContact;
	}
	public void setProPhone(String proPhone){
	this.proPhone=proPhone;
	}
	public String getProPhone(){
		return proPhone;
	}
	public void setProAddress(String proAddress){
	this.proAddress=proAddress;
	}
	public String getProAddress(){
		return proAddress;
	}
	public void setProFax(String proFax){
	this.proFax=proFax;
	}
	public String getProFax(){
		return proFax;
	}
	public void setCreatedBy(long createdBy){
	this.createdBy=createdBy;
	}
	public long getCreatedBy(){
		return createdBy;
	}
	public void setCreationDate(Date creationDate){
	this.creationDate=creationDate;
	}
	public Date getCreationDate(){
		return creationDate;
	}
	public void setModifyDate(Date modifyDate){
	this.modifyDate=modifyDate;
	}
	public Date getModifyDate(){
		return modifyDate;
	}
	public void setModifyBy(long modifyBy){
	this.modifyBy=modifyBy;
	}
	public long getModifyBy(){
		return modifyBy;
	}
	public Provider() {
		super();
	}
	public Provider(String proCode, String proName, String proDesc, String proContact, String proPhone,
			String proAddress, String proFax, long createdBy, Date creationDate) {
		super();
		this.proCode = proCode;
		this.proName = proName;
		this.proDesc = proDesc;
		this.proContact = proContact;
		this.proPhone = proPhone;
		this.proAddress = proAddress;
		this.proFax = proFax;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
	}
	public Provider(long id, String proCode, String proName, String proDesc, String proContact, String proPhone,
			String proAddress, String proFax, Date modifyDate, long modifyBy) {
		super();
		this.id = id;
		this.proCode = proCode;
		this.proName = proName;
		this.proDesc = proDesc;
		this.proContact = proContact;
		this.proPhone = proPhone;
		this.proAddress = proAddress;
		this.proFax = proFax;
		this.modifyDate = modifyDate;
		this.modifyBy = modifyBy;
	}
	
	
}

