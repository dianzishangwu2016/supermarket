package com.wskj.supermarket.pojo;

import java.util.Date;

/**
 * Bill 实体类     账单实体类
 * Tue Oct 17 18:24:21 CST 2017 
 */ 
public class Bill{
	private long id;//账单的id
	private String billCode;//编号
	private String productName;//商品的名称
	private long supplierId;//供应商的id
	private String supplierName;//供应商的名称
	private String productDesc;//商品描述
	private String productUnit;//单位
	private double productCount;//数量
	private double totalPrice;//总价格
	private int isPayment;// 0未付款  1已经付款
	private long createdBy; //创建人
	private Date creationDate; //创建的时间 
	private long modifyBy;//修改人
	private Date modifyDate; //修改的时间
	
	
	
	
	public long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public void setId(long id){
	this.id=id;
	}
	public long getId(){
		return id;
	}
	public void setBillCode(String billCode){
	this.billCode=billCode;
	}
	public String getBillCode(){
		return billCode;
	}
	public void setProductName(String productName){
	this.productName=productName;
	}
	public String getProductName(){
		return productName;
	}
	public void setProductDesc(String productDesc){
	this.productDesc=productDesc;
	}
	public String getProductDesc(){
		return productDesc;
	}
	public void setProductUnit(String productUnit){
	this.productUnit=productUnit;
	}
	public String getProductUnit(){
		return productUnit;
	}
	public void setProductCount(double productCount){
	this.productCount=productCount;
	}
	public double getProductCount(){
		return productCount;
	}
	public void setTotalPrice(double totalPrice){
	this.totalPrice=totalPrice;
	}
	public double getTotalPrice(){
		return totalPrice;
	}
	public void setIsPayment(int isPayment){
	this.isPayment=isPayment;
	}
	public int getIsPayment(){
		return isPayment;
	}
	public void setCreatedBy(long createdBy){
	this.createdBy=createdBy;
	}
	public long getCreatedBy(){
		return createdBy;
	}
	public void setCreationDate(Date creationDate){
	this.creationDate=creationDate;
	}
	public Date getCreationDate(){
		return creationDate;
	}
	public void setModifyBy(long modifyBy){
	this.modifyBy=modifyBy;
	}
	public long getModifyBy(){
		return modifyBy;
	}
	public void setModifyDate(Date modifyDate){
	this.modifyDate=modifyDate;
	}
	public Date getModifyDate(){
		return modifyDate;
	}
	public Bill() {
		super();
	}
	public Bill(String billCode, String productName, long supplierId, String productUnit, double productCount,
			double totalPrice, int isPayment, long createdBy, Date creationDate) {
		super();
		this.billCode = billCode;
		this.productName = productName;
		this.supplierId = supplierId;
		this.productUnit = productUnit;
		this.productCount = productCount;
		this.totalPrice = totalPrice;
		this.isPayment = isPayment;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
	}
	public Bill(long id, String billCode, String productName, long supplierId, String productUnit, double productCount,
			double totalPrice, int isPayment, long modifyBy, Date modifyDate) {
		super();
		this.id = id;
		this.billCode = billCode;
		this.productName = productName;
		this.supplierId = supplierId;
		this.productUnit = productUnit;
		this.productCount = productCount;
		this.totalPrice = totalPrice;
		this.isPayment = isPayment;
		this.modifyBy = modifyBy;
		this.modifyDate = modifyDate;
	}
	
	
	
	
	
}

