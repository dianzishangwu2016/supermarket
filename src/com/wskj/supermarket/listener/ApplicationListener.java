package com.wskj.supermarket.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *  项目启动的时候加载  项目的名称路径 供页面使用
 * @author Administrator
 *
 */
@WebListener
public class ApplicationListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		// TODO Auto-generated method stub
		
		ServletContext applicaiton = event.getServletContext();
        applicaiton.setAttribute("basePath",applicaiton.getContextPath());
	    applicaiton.setAttribute("company","河北交通职业技术学院电子商务2016级");
	}

}
