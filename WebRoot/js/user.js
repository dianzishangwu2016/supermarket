/**
 * 
 */

$(function(){
	
	$(".left .list li:eq(2)").attr("id","active");
	
	 $("#userId").blur(function(){
	       //判断是否已经存在这个用户  ajax 提交后台的进行验证 
	       var obj=this;
		   var userCode=this.value;
	       var url="${basePath}/user/checkUserCode.do";
	       
	       $.getJSON(url,{"code":obj.value},function(result){    	    	   
	    	   //console.log(result);
	    	   //已经存在  就提示更换用户编号 否则就可以注册
	    	   if(result){
	    		  alert("用户名已经存在！请更换其他的用户名！");
	    		  obj.value="";    	    		   
	    	   }
	    	   
	       });    		  
	  });
	  
    //判断两次输入的密码是否相同
	   $("#userRemi").blur(function(){
		   
		   var repass=this.value;
		   var pass=$("#userpassword").val();
		   
		   if(repass==null ||repass.trim()==""){
			   $(this).next().html("请输入确认密码");
			   this.value="";
			   this.focus();
			   
		   }else{
			   // 如果两次输入的密码不相同 ，就重新输入密码
			   if(repass!=pass){
				   alert("两次输入的密码不相同，请重新输入！"); 
				   this.value="";
				   $("#userpassword").val("");
				   $("#userpassword").focus();    				   
			   }   			   
		   }
		   
		   
	   });    
	 
	
	
	
});