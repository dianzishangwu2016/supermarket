<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
   <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
<%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
<section class="publicMian ">
    <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面 >> 用户信息查看页面</span>
        </div>
        <div class="providerView">
            <p><strong>用户编号：</strong><span>${user.userCode }</span></p>
            <p><strong>用户名称：</strong><span>${user.userName }</span></p>
            <p><strong>用户性别：</strong><span>${user.gender==1?"男":"女" }</span></p>
            <p><strong>出生日期：</strong><span>${user.birthday }</span></p>
            <p><strong>用户电话：</strong><span>${user.phone }</span></p>
            <p><strong>用户地址：</strong><span>${user.address }</span></p>
            <p><strong>用户类别：</strong><span>${user.userType==1?"总经理":(user.userType==2?"经理":"普通员工") }</span></p>

            <a href="${basePath }/user/list.do">返回</a>
        </div>
    </div>
</section>
<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
  <script src="${basePath }/js/user.js"></script>

</body>
</html>