<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>    
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
<%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
<section class="publicMian ">
     <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面 >> 用户修改页面</span>
        </div>
        <div class="providerAdd">
            <form action="${basePath }/user/update.do" method="post">
                <input type="hidden" name="id" value="${user.id }" />
                <!--div的class 为error是验证错误，ok是验证成功-->                
                <div>
                    <label for="userName">用户名称：</label>
                    <input type="text" name="userName" id="userName" value="${user.userName }"/>
                    <span >*</span>
                </div>

                <div>
                    <label >用户性别：</label>

                    <select name="gender">                  
                        <option value="1"  <c:if test="${user.gender==1 }">selected</c:if>>男</option>
                        <option value="2"  <c:if test="${user.gender==2 }">selected</c:if>>女</option>
                    </select>
                </div>
                <div>
                    <label for="date">出生日期：</label>
                    <input type="text" name="date" id="date" value="${user.birthday }"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="userphone">用户电话：</label>
                    <input type="text" name="userphone" id="userphone" value="${user.phone}"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="userAddress">用户地址：</label>
                    <input type="text" name="userAddress" id="userAddress" value="${user.address }"/>
                </div>
                <div>
                    <label >用户类别：</label>
                    
                    <input type="radio" name="userlei" value="1" <c:if test="${user.userType==1 }">checked</c:if>/>管理员
                    <input type="radio" name="userlei" value="2" <c:if test="${user.userType==2 }">checked</c:if>/>经理
                    <input type="radio" name="userlei" value="3" <c:if test="${user.userType==3 }">checked</c:if>/>普通用户

                </div>
                <div class="providerAddBtn">
                    <!--<a href="#">保存</a>-->
                    <!--<a href="userList.html">返回</a>-->
                    <input type="submit" value="保存"/>
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>

    </div>
</section>
<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
 <script src="${basePath }/js/user.js"></script>
</body>
</html>