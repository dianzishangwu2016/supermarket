<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
  <!--  头部的信息,公共的页面   -->
  <%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
    <section class="publicMian ">
        <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
        
        <div class="right">
            <div class="location">
                <strong>你现在所在的位置是:</strong>
                <span>用户管理页面</span>
            </div>
            <div class="search">
            <form action="${basePath }/user/list.do" method="post">            
                <span>用户名：</span>
                <input type="text" placeholder="请输入用户名" name="uname" value="${param.uname }"/>
                <input type="submit" value="查询"/>                
                <a href="${basePath }/user/add.do">添加用户</a>
             </form>
            </div>
            <!--用户-->
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tr class="firstTr">
                    <th width="5%">序号</th>
                    <th width="5%">用户编码</th>
                    <th width="20%">用户名称</th>
                    <th width="10%">性别</th>
                    <th width="10%">年龄</th>
                    <th width="10%">电话</th>
                    <th width="10%">用户类型</th>
                    <th width="30%">操作</th>
                </tr>
                <c:forEach items="${users }" var="user" varStatus="status">
                <tr>
                    <td>${status.count }</td>
                    <td>${user.userCode }</td>
                    <td>${user.userName }</td>
                    <td>${user.gender==1?"男":"女" }</td>
                    <td>${user.age }</td>
                    <td>${user.phone }</td>
                    <td>${user.typeName }</td>
                    <td>
                        <a href="${basePath }/user/view.do?id=${user.id }"><img src="${basePath }/img/read.png" alt="查看" title="查看"/></a>
                        <a href="${basePath }/user/update.do?id=${user.id }"><img src="${basePath }/img/xiugai.png" alt="修改" title="修改"/></a>
                        <a href="${basePath }/user/delete.do?id=${user.id }" onclick="return confirm('确认删除？删除后无法恢复')" ><img src="${basePath }/img/schu.png" alt="删除" title="删除"/></a>
                    	<a href="${basePath }/user/resetPassword.do?id=${user.id }" onclick="return confirm('重值密码为000000')" ><img src="${basePath }/img/resetPassword.png" alt="重置密码" title="重置密码"/></a>
                    </td>
                </tr>
                </c:forEach>                
            </table>
        </div>
    </section>

  <!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>

<script src="${basePath }/js/user.js"></script>

</body>
</html>