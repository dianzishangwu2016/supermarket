<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
    <%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
    <section class="publicMian ">
         <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
        <div class="right">
            <div class="location">
                <strong>你现在所在的位置是:</strong>
                <span>账单管理页面</span>
            </div>
            <div class="search">
            <form action="${basePath }/bill/list.do" method="post">
                <span>商品名称：</span>
                <input type="text" name="gname" value="${param.gname }" placeholder="请输入商品的名称"/>
                
                <span>供应商：</span>
                <select name="sid" >
                    <option value="-1">--请选择--</option>
                    <c:forEach items="${providers }" var="pro">                    
                       <option value="${pro.id }" <c:if test="${pro.id==param.sid }">selected</c:if> >
                       ${pro.proName }
                       </option>
                    </c:forEach>
                    <option value="">邯郸市五得利面粉厂</option>
                </select>

                <span>是否付款：</span>
                <select name="fukuan">              
                    <option value="-1">--请选择--</option>
                    <option value="1"   <c:if test="${param.fukuan==1 }">selected</c:if>>已付款</option>
                    <option value="0"   <c:if test="${param.fukuan==0 }">selected</c:if>>未付款</option>
                </select>

                <input type="submit" value="查询"/>
            </form>
                <a href="${basePath }/bill/add.do">添加订单</a>
            </div>
            <!--账单表格 样式和供应商公用-->
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tr class="firstTr">
                    <th width="5%">序号</th>
                    <th width="5%">账单编码</th>
                    <th width="20%">商品名称</th>
                    <th width="10%">供应商</th>
                    <th width="10%">账单金额</th>
                    <th width="10%">是否付款</th>
                    <th width="10%">创建时间</th>
                    <th width="30%">操作</th>
                </tr>
                
                <c:forEach items="${bills }" var="bill" varStatus="status">         
                            
                <tr>
                    <td>${status.count }</td>
                    <td>${bill.billCode }</td>
                    <td>${bill.productName }</td>
                    <td>${bill.supplierName }</td>
                    <td>${bill.totalPrice }</td>
                    <td>${bill.isPayment==1?"已付款":"未付款" }</td>
                    <td>${bill.creationDate}</td>
                    <td>
                        <a href="${basePath }/bill/view.do?id=${bill.id}"><img src="img/read.png" alt="查看" title="查看"/></a>
                        <a href="${basePath }/bill/update.do?id=${bill.id}"><img src="img/xiugai.png" alt="修改" title="修改"/></a>
                        <a href="${basePath }/bill/delete.do?id=${bill.id}" onclick="return confirm('确认删除？')"><img src="img/schu.png" alt="删除" title="删除"/></a>
                    </td>
                </tr>
                </c:forEach> 
            </table>
        </div>
    </section>


    <!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
  <script src="${basePath }/js/bill.js"></script>
</body>
</html>