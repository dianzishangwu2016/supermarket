<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>     
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
   <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
<%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
<section class="publicMian ">
     <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>账单管理页面 >> 订单添加页面</span>
        </div>
        <div class="providerAdd">
            <form action="${basePath }/bill/update.do" method="post">
            <input type="hidden" name="id" value="${bill.id }">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <div class="">
                    <label for="billId">订单编码：</label>
                    <input type="text" name="billId" id="billId" value="${bill.billCode }" readonly />
                    <span>*请输入订单编码</span>
                </div>
                <div>
                    <label for="billName">商品名称：</label>
                    <input type="text" name="billName" id="billName" value="${bill.productName }" required/>
                    <span >*请输入商品名称</span>
                </div>
                <div>
                    <label for="billCom">商品单位：</label>
                    <input type="text" name="billCom" id="billCom" value="${bill.productUnit }" required/>
                    <span>*请输入商品单位</span>

                </div>
                <div>
                    <label for="billNum">商品数量：</label>
                    <input type="text" name="billNum" id="billNum" value="${bill.productCount }" required/>
                    <span>*请输入大于0的正自然数，小数点后保留2位</span>
                </div>
                <div>
                    <label for="money">总金额：</label>
                    <input type="text" name="money" id="money" value="${bill.totalPrice }" required/>
                    <span>*请输入大于0的正自然数，小数点后保留2位</span>
                </div>
                <div>
                    <label for="fax">供应商：</label>
                    <select name="supplier" >                        
                        <c:forEach items="${providers }" var="pro">                        
                        <option value="${pro.id }" <c:if test="${bill.supplierId==pro.id }">selected</c:if> >
                        ${pro.proName }
                        </option>                     
                        </c:forEach>                        
                    </select>                   
                    <span>*</span>
                </div>
                <div>
                    <label >是否付款：</label>
                    
                    <input type="radio" name="zhifu" value="0" <c:if test="${bill.isPayment==0 }">checked</c:if>/>未付款
                    <input type="radio" name="zhifu" value="1" <c:if test="${bill.isPayment==1 }">checked</c:if>/>已付款
                </div>
                <div class="providerAddBtn">
                    
                    <input type="submit" value="修改" />
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>

    </div>
</section>
<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
   <script src="${basePath }/js/bill.js"></script>
</body>
</html>