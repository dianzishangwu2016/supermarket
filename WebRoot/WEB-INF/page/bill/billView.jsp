<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>超市账单管理系统</title>
<%@ include file="/WEB-INF/page/comm.jsp"%>
</head>
<body>
	<!--头部-->
	<%@ include file="/WEB-INF/page/header.jsp"%>
	<!--主体内容-->
	<section class="publicMian ">
		<!--  左侧的菜单栏,公共的页面 -->
		<%@ include file="/WEB-INF/page/left.jsp"%>
		<div class="right">
			<div class="location">
				<strong>你现在所在的位置是:</strong> <span>账单管理页面 >> 信息查看</span>
			</div>
			<div class="providerView">
				<p>
					<strong>订单编号：</strong><span>${bill.billCode }</span>
				</p>
				<p>
					<strong>商品名称：</strong><span>${bill.productName }</span>
				</p>
				<p>
					<strong>商品单位：</strong><span>${bill.productUnit }</span>
				</p>
				<p>
					<strong>商品数量：</strong><span>${bill.productCount }</span>
				</p>
				<p>
					<strong>总金额：</strong><span>${bill.totalPrice }</span>
				</p>
				<p>
					<strong>供应商：</strong><span>${bill.supplierName  }</span>
				</p>
				<p>
					<strong>是否付款：</strong><span>${bill.isPayment==1?"已付款":"未付款"  }</span>
				</p>

				<a href="${basePath }/bill/list.do">返回</a>
			</div>
		</div>
	</section>
	<!--  底部的信息，页面都是一样共用 -->
	<%@ include file="/WEB-INF/page/footer.jsp"%>
	<script src="${basePath }/js/bill.js"></script>

</body>
</html>