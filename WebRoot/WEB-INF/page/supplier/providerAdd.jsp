<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>超市账单管理系统</title>
<%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
	<!--头部-->
	<%@ include file="/WEB-INF/page/header.jsp" %>
	<!--主体内容-->
	<section class="publicMian ">
		 <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
		<div class="right">
			<div class="location">
				<strong>你现在所在的位置是:</strong> <span>供应商管理页面 >> 供应商添加页面</span>
			</div>
			<div class="providerAdd">
				<form action="${basePath }/supplier/add.do" method="post">
					<!--div的class 为error是验证错误，ok是验证成功-->
					<div class="">
						<label for="providerId">供应商编码：</label> <input type="text"
							name="providerCode" id="providerId" required/> <span>*请输入供应商编码</span>
					</div>
					<div>
						<label for="providerName">供应商名称：</label> <input type="text"
							name="providerName" id="providerName" required /> <span>*请输入供应商名称</span>
					</div>
					<div>
						<label for="people">联系人：</label> 
						<input type="text" name="proConcat" id="people" required/>
						 <span>*请输入联系人</span>

					</div>
					<div>
						<label for="phone">联系电话：</label> <input type="text" name="phone"
							id="phone" required/> <span>*请输入联系电话</span>
					</div>
					<div>
						<label for="address">联系地址：</label> <input type="text"
							name="address" id="address" /> <span></span>
					</div>
					<div>
						<label for="fax">传真：</label> <input type="text" name="fax"
							id="fax" /> <span></span>
					</div>
					<div>
						<label for="describe">描述：</label> <input type="text"
							name="describe" id="describe" />
					</div>
					<div class="providerAddBtn">						
						<input type="submit" value="保存"  /> 
						<input type="button" value="返回" onclick="history.back(-1)" />
					</div>
				</form>
			</div>

		</div>
	</section>
	<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
  <script type="text/javascript" src="${basePath }/js/provier.js"></script>
</body>
</html>