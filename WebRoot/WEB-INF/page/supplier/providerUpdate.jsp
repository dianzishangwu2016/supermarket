<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
<%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
<section class="publicMian ">
     <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>供应商管理页面 >> 供应商修改页</span>
        </div>
        <div class="providerAdd">
            <form action="${basePath }/supplier/update.do" method="post">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <input type="hidden" name="id" value="${pro.id }"/>
                <div class="">
                    <label for="providerId">供应商编码：</label>
                    <input type="text" name="providerCode" id="providerId" value="${pro.proCode }"/>
                    <span>*</span>
                </div>
                <div>
                    <label for="providerName">供应商名称：</label>
                    <input type="text" name="providerName" id="providerName" value="${pro.proName }"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="people">联系人：</label>
                    <input type="text" name="proConcat" id="people" value="${pro.proContact }"/>
                    <span>*</span>

                </div>
                <div>
                    <label for="phone">联系电话：</label>
                    <input type="text" name="phone" id="phone" value="${pro.proPhone }"/>
                    <span></span>
                </div>
                <div>
                    <label for="address">联系地址：</label>
                    <input type="text" name="address" id="address" value="${pro.proAddress }"/>
                    <span></span>

                </div>
                <div>
                    <label for="fax">传真：</label>
                    <input type="text" name="fax" id="fax" value="${pro.proFax }"/>
                    <span></span>

                </div>
                <div>
                    <label for="describe">描述：</label>
                    <input type="text" name="describe" id="describe" value="${pro.proDesc }"/>
                    <span></span>

                </div>
                <div class="providerAddBtn">                    
                    <input type="submit" value="修改" />
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>

    </div>
</section>
<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
  <script type="text/javascript" src="${basePath }/js/provider.js"></script>

</body>
</html>