<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
<%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
<section class="publicMian ">
     <!--  左侧的菜单栏,公共的页面 -->
     <%@ include file="/WEB-INF/page/left.jsp" %>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>供应商管理页面 >> 信息查看</span>
        </div> 
        
        <div class="providerView">
            <p><strong>供应商编码：</strong><span>${pro.proCode }</span></p>
            <p><strong>供应商名称：</strong><span>${pro.proName }</span></p>
            <p><strong>联系人：</strong><span>${pro.proContact }</span></p>
            <p><strong>联系电话：</strong><span>${pro.proPhone }</span></p>
            <p><strong>传真：</strong><span>${pro.proFax }</span></p>
            <p><strong>描述：</strong><span>${pro.proDesc }</span></p>

            <a href="${basePath }/supplier/list.do">返回</a>
        </div>
    </div>
</section>
<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
  <script type="text/javascript" src="${basePath }/js/provier.js"></script>

</body>
</html>