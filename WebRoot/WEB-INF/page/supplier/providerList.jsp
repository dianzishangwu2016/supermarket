<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>    
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <%@ include file="/WEB-INF/page/comm.jsp" %>
</head>
<body>
<!--头部-->
<%@ include file="/WEB-INF/page/header.jsp" %>
<!--主体内容-->
<section class="publicMian ">
     <!--  左侧的菜单栏,公共的页面 -->
        <%@ include file="/WEB-INF/page/left.jsp" %>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>供应商管理页面</span>
        </div>
        <div class="search">
         <form action="${basePath }/supplier/list.do" method="post">
            <span>供应商名称：</span>
            <input type="text" name="name" value="${param.name }" placeholder="请输入供应商的名称"/>
            <input type="submit" value="查询"/>
         </form>
            <a href="${basePath}/supplier/add.do">添加供应商</a>
        </div>
        <!--供应商操作表格-->
        <table class="providerTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
            	<th width="5%">序号</th>
                <th width="5%">供应商编码</th>
                <th width="20%">供应商名称</th>
                <th width="10%">联系人</th>
                <th width="10%">联系电话</th>
                <th width="10%">传真</th>
                <th width="10%">创建时间</th>
                <th width="30%">操作</th>
            </tr>
            <c:forEach items="${list }" var="pro" varStatus="status">
            
           
            <tr>
                <td>${status.count }</td>
                <td>${pro.proCode }</td>
                <td>${pro.proName }</td>
                <td>${pro.proContact }</td>
                <td>${pro.proPhone }</td>
                <td>${pro.proFax }</td>
                <td>${pro.creationDate }</td>
                <td>
                    <a href="${basePath}/supplier/view.do?id=${pro.id}"><img src="img/read.png" alt="查看" title="查看"/></a>
                    <a href="${basePath}/supplier/update.do?id=${pro.id}"><img src="img/xiugai.png" alt="修改" title="修改"/></a>
                    <a href="${basePath}/supplier/delete.do?id=${pro.id}"  onclick="return confirm('确认删除？')"><img src="img/schu.png" alt="删除" title="删除"/></a>
                </td>
            </tr>
           </c:forEach>           
        </table>
    </div>
</section>

<!--  底部的信息，页面都是一样共用 -->
  <%@ include file="/WEB-INF/page/footer.jsp" %>
  <script type="text/javascript" src="${basePath }/js/provier.js"></script>

</body>
</html>