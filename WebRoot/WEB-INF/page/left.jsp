<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="left">
     <h2 class="leftH2"><span class="span1"></span>功能列表 <span></span></h2>
     <nav>
         <ul class="list">
             <li><a href="${basePath }/bill/list.do">账单管理</a></li>
             <li><a href="${basePath }/supplier/list.do">供应商管理</a></li>
             <li><a href="${basePath }/user/list.do">用户管理</a></li>
             <li><a href="${basePath }/updatePassword.do">密码修改</a></li>
             <li><a href="${basePath }/loginOut.do">退出系统</a></li>
         </ul>
     </nav>
</div>