/*
SQLyog Ultimate v11.27 (32 bit)
MySQL - 5.5.28-log : Database - smbms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`smbms` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `smbms`;

/*Table structure for table `bill` */

DROP TABLE IF EXISTS `bill`;

CREATE TABLE `bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `billCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '账单编码',
  `supplierId` bigint(20) DEFAULT NULL,
  `productName` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品名称',
  `productDesc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品描述',
  `productUnit` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品单位',
  `productCount` decimal(20,2) DEFAULT NULL COMMENT '商品数量',
  `totalPrice` decimal(20,2) DEFAULT NULL COMMENT '商品总额',
  `isPayment` int(10) DEFAULT NULL COMMENT '是否支付（0：未支付 1：已支付）',
  `createdBy` bigint(20) DEFAULT NULL COMMENT '创建者（userId）',
  `creationDate` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyBy` bigint(20) DEFAULT NULL COMMENT '更新者（userId）',
  `modifyDate` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `supplierId` (`supplierId`),
  CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`supplierId`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `bill` */

insert  into `bill`(`id`,`billCode`,`supplierId`,`productName`,`productDesc`,`productUnit`,`productCount`,`totalPrice`,`isPayment`,`createdBy`,`creationDate`,`modifyBy`,`modifyDate`) values (1,'bg20171014',1,'花生米','bghsm20161214','kg','40.00','500.00',1,1,'2016-12-15 00:00:00',NULL,NULL),(3,'sg20171018',2,'火腿肠',NULL,'箱','200.00','7000.00',0,1,'2017-10-20 22:55:08',1,'2017-10-20 23:03:05'),(4,'hl20171018',4,'哇哈哈',NULL,'箱','80.00','8000.00',0,1,'2017-10-20 22:55:53',1,'2017-10-20 23:03:10'),(5,'sg20171019',2,'鸡肉肠',NULL,'kg','800.00','4800.00',0,1,'2017-10-20 23:04:00',NULL,NULL),(6,'sg20171021',2,'双鸽蛋饼',NULL,'个','1000.00','2000.00',0,1,'2017-10-21 10:06:00',NULL,NULL);

/*Table structure for table `con_test` */

DROP TABLE IF EXISTS `con_test`;

CREATE TABLE `con_test` (
  `a` char(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `con_test` */

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `logId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_name` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `level` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thread_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `all_category` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `logs` */

/*Table structure for table `provider` */

DROP TABLE IF EXISTS `provider`;

CREATE TABLE `provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `proCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商编码',
  `proName` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商名称',
  `proDesc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商详细描述',
  `proContact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商联系人',
  `proPhone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系电话',
  `proAddress` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `proFax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '传真',
  `createdBy` bigint(20) DEFAULT NULL COMMENT '创建者（userId）',
  `creationDate` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyDate` datetime DEFAULT NULL COMMENT '更新时间',
  `modifyBy` bigint(20) DEFAULT NULL COMMENT '更新者（userId）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `provider` */

insert  into `provider`(`id`,`proCode`,`proName`,`proDesc`,`proContact`,`proPhone`,`proAddress`,`proFax`,`createdBy`,`creationDate`,`modifyDate`,`modifyBy`) values (1,'beiguo','北国商城','sn001','张三','13585211452','河北省石家庄市','0311-84848521',1,'2010-10-05 00:00:00',NULL,NULL),(2,'shuangge','双鸽冷藏食品','sn002','李四','13685412145','河北省石家庄市','0311-84587412',1,'2011-10-06 00:00:00',NULL,NULL),(3,'xd20171452','河北兴达食品','有机食品，玉米面','胡秋月','15852147412','河北省石家庄藁城区','0311-85414754',1,'2017-10-21 10:02:42',NULL,NULL),(4,'xgtg20171452','兴国甜品糖果有限公司','专业糖果生成','徐兴国','13652145741','河北省唐山市','0311-87412541',1,'2017-10-21 10:04:59',NULL,NULL);

/*Table structure for table `userinfo` */

DROP TABLE IF EXISTS `userinfo`;

CREATE TABLE `userinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `userCode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户编码',
  `userName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名称',
  `userPassword` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户密码',
  `gender` int(10) DEFAULT NULL COMMENT '性别（1:女、 2:男）',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机',
  `address` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '地址',
  `userType` int(10) DEFAULT NULL COMMENT '用户类型（1：系统管理员、2：经理、3：普通员工）',
  `createdBy` bigint(20) DEFAULT NULL COMMENT '创建者（userId）',
  `creationDate` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyBy` bigint(20) DEFAULT NULL COMMENT '更新者（userId）',
  `modifyDate` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `userinfo` */

insert  into `userinfo`(`id`,`userCode`,`userName`,`userPassword`,`gender`,`birthday`,`phone`,`address`,`userType`,`createdBy`,`creationDate`,`modifyBy`,`modifyDate`) values (1,'admin','张三峰','ISMvKXpXpadDiUoOSoAfww==',1,'1991-10-01','13545145145','河北省石家庄',1,1,'2010-10-10 00:00:00',NULL,NULL),(2,'zhangsan','张三','Adf0B2CWDnvZRDUT8iq5rw==',1,'1992-05-14','13851452145','河北省保定市',2,1,'2010-08-08 00:00:00',NULL,NULL),(4,'wangwu','王五','ZwsUcorZkCrsujLiL6T2vQ==',1,'1993-10-14','13652145412','河北省石家庄市',2,1,'2011-09-09 00:00:00',NULL,NULL),(5,'zsf','张三丰','ZwsUcorZkCrsujLiL6T2vQ==',1,'1996-10-14','13696365211','河北省石家庄市',2,1,'2012-10-01 00:00:00',NULL,NULL),(6,'lxy','李逍遥','111',1,'1995-04-08','13965847414','河北省石家庄鹿泉区',3,1,'2012-08-09 00:00:00',NULL,NULL),(7,'zwj','张无忌','111',1,'1996-08-09','13754121452','河北省邢台市',2,1,'2012-09-09 00:00:00',NULL,NULL),(8,'zm','赵敏','111',2,'1996-10-14','13852411452','河北省石家庄无极限',3,1,'2012-08-08 00:00:00',NULL,NULL),(9,'zyh','张义环','111',2,'1998-08-09','13415214152','河北省唐山市',3,1,'2014-08-09 00:00:00',NULL,NULL);

/*Table structure for table `usertype` */

DROP TABLE IF EXISTS `usertype`;

CREATE TABLE `usertype` (
  `typeId` int(11) NOT NULL AUTO_INCREMENT,
  `typeName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `usertype` */

insert  into `usertype`(`typeId`,`typeName`) values (1,'总经理'),(2,'部门经理'),(3,'员工');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
